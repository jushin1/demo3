package ms.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @PostMapping("login")
    public String login(String email, String password) {

        System.out.println(email);
        System.out.println(password);

        return "redirect:/home";
    }
}
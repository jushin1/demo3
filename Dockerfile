FROM java

MAINTAINER mgjung <mgjung@rockplace.co.kr>

COPY ./build/libs/demo-0.0.1-SNAPSHOT.jar /home/demo-0.0.1-SNAPSHOT.jar
EXPOSE 8080
CMD java -jar /home/demo-0.0.1-SNAPSHOT.jar